# speeddisplay

The purpose of this repository is to build an open source speed display for private usage. The goal is to detect a car or truck and visualize the driver a sign, that the driver should drive carefully because there are playing children. With this repository everybody should be able to build his own speed display.

At the moment this project is at the beginning so at the moment there is no running solution, but feel free to contribute and help me.

## Components

* [Arduino UNO](https://www.arduino.cc/en/Main/arduinoBoardUno)
* [Radar Sensor IPM-165 incl. amplification](https://shop.weidmann-elektronik.de/index.php?page=product&info=8)
